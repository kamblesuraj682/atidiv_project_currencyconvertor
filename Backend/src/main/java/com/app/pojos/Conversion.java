package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Conversion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(length = 20, nullable = false)
	private String country1;
	
	@Column(length = 20, nullable = false)
	private String country2;
	
	@Column(nullable = false)
	private float value1;
	
	@Column(nullable = false)
	private float value2;
	
	@ManyToOne
	@JoinColumn(name = "userId")
	private User userId;

	
	public Conversion() {
		System.out.println("in coversion ctr ..");
	}
	
	public Conversion(String country1, String country2, float value1, float value2) {
		super();
		this.country1 = country1;
		this.country2 = country2;
		this.value1 = value1;
		this.value2 = value2;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCountry1() {
		return country1;
	}

	public void setCountry1(String country1) {
		this.country1 = country1;
	}

	public String getCountry2() {
		return country2;
	}

	public void setCountry2(String country2) {
		this.country2 = country2;
	}

	public float getValue1() {
		return value1;
	}

	public void setValue1(float value1) {
		this.value1 = value1;
	}

	public float getValue2() {
		return value2;
	}

	public void setValue2(float value2) {
		this.value2 = value2;
	}

	public User getUserId() {
		return userId;
	}

	public void setUserId(User userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Conversion [id=" + id + ", country1=" + country1 + ", country2=" + country2 + ", value1=" + value1
				+ ", value2=" + value2 + ", userId=" + userId + "]";
	}
	
	
	
	
}
