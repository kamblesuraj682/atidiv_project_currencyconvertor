package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IConversionDao;

import com.app.pojos.Conversion;


@Service
@Transactional
public class ConversionServiceImpl implements IConversionService {

	
	@Autowired
	private IConversionDao dao;
	
	
	@Override
	public void addConversion(int id, Conversion c) {
		dao.addConversion(id, c);

	}

	@Override
	public List<Conversion> getConversionsByUserId(int userId) {
		return dao.getConversionsByUserId(userId);
	}

}
