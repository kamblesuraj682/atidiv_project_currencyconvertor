package com.app.service;

import java.util.List;

import com.app.pojos.Conversion;

public interface IConversionService {

	void addConversion(int id, Conversion c);
	
	List<Conversion> getConversionsByUserId(int userId);
}
