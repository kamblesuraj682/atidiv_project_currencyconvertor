package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Conversion;
import com.app.service.IConversionService;

@RestController
@CrossOrigin("*")
@RequestMapping("/conversion")
public class ConversionController {
	
	@Autowired
	private IConversionService conversion;
	
	
	@PostMapping("/addConversion/{userId}")
	public ResponseEntity<?> addConversionById(@PathVariable int userId,@RequestBody Conversion c) {
		System.out.println("addConversionById");
		try {
			conversion.addConversion(userId, c);
			return new ResponseEntity<>("conversion is added successfully", HttpStatus.OK);
		} catch (RuntimeException e) {
			e.printStackTrace();
			return new ResponseEntity<>("conversion is not added", HttpStatus.OK);
		}
	}
	
	
	@GetMapping("/convertor/{userId}")
	public ResponseEntity<?> getAllConversionById(@PathVariable int userId)
	{
		System.out.println("in getAllConversionById  ");
		try {
			return new ResponseEntity<>(conversion.getConversionsByUserId(userId), HttpStatus.OK);
		} catch (RuntimeException e) {
			return new ResponseEntity<>("Empty conversion list", HttpStatus.OK);
		}
	}
	
}
