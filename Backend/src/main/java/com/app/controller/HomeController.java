package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.User;
import com.app.service.IUserService;

@RestController
@CrossOrigin("*")
public class HomeController {
	@Autowired
	private IUserService userService;

	public HomeController() {
		System.out.println("in home controller");
	}

	
	//-----------------------------Login------------------------------------------------------------------------------------------------------------------

		@PostMapping("/login")
		public ResponseEntity<?> validateUser(@RequestBody User u) {
			System.out.println("in valditae user" + u.getEmail() + "," + u.getPassword());
			try {
				return new ResponseEntity<>(userService.authenticateUser(u.getEmail(), u.getPassword()), HttpStatus.OK);
			} catch (RuntimeException e) {
				return new ResponseEntity<>("Invalid Password or email...Please enter valid data", HttpStatus.OK);
			}
		}
	//----------------------------Registration-------------------------------------------------------------------------------------------------------------------

		@PostMapping("/register")
		public ResponseEntity<?> registerUser(@RequestBody User u) {
			System.out.println("in register user");
			try {
				System.out.println(u);
				userService.registerUser(u);
				return new ResponseEntity<>("Registeration Done Succssfully", HttpStatus.OK);
			} catch (RuntimeException e) {
				return new ResponseEntity<>("Registeration Fail....", HttpStatus.OK);
			}
		}
}
