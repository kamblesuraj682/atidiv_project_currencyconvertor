package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Conversion;
import com.app.pojos.User;

@Repository
public class ConversionDaoImpl implements IConversionDao {

	
	@Autowired
	private EntityManager mgr;
	
	@Override
	public void addConversion(int id, Conversion c) {
		String jpql = "select u from User u where u.id=:i";
		User u = mgr.createQuery(jpql, User.class).setParameter("i", id).getSingleResult();
		c.setUserId(u);
		mgr.persist(c);

	}

	@Override
	public List<Conversion> getConversionsByUserId(int userId) {
		String jpql = "select c from Conversion c where c.userId=:user limit 10";
		
		User u = mgr.find(User.class, userId);
		List<Conversion> list = mgr.createQuery(jpql, Conversion.class).setParameter("user", u).getResultList();
		return list;
	}
	
	

}
