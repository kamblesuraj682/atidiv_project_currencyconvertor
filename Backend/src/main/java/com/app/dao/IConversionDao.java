package com.app.dao;

import java.util.List;

import com.app.pojos.Conversion;


public interface IConversionDao {

	void addConversion(int id, Conversion c);
	
	List<Conversion> getConversionsByUserId(int userId);
}
